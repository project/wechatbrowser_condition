<?php

namespace Drupal\wechatbrowser_condition\Plugin\Commerce\Condition;

use Drupal\commerce\Plugin\Commerce\Condition\ConditionBase;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a browser agent condition for Wechat Payment methods.
 *
 * @CommerceCondition(
 *   id = "not_in_wechatbrowser_condition",
 *   label = @Translation("Not In Wechat App built-in browser"),
 *   display_label = @Translation("Only show NOT in Wechat Browser"),
 *   category = @Translation("Browser context"),
 *   entity_type = "commerce_order",
 * )
 */
class NotWechatBrowser extends ConditionBase {

  /**
   * {@inheritdoc}
   */
  public function evaluate(EntityInterface $entity) {
    $this->assertEntity($entity);
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */

    return strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'micromessenger') === FALSE;
  }
}
